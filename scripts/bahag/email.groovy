//v0.0.4

import de.eddyson.filterconditions.FilterConditionsSerializerJSON
import org.apache.poi.xssf.usermodel.XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import javax.activation.DataHandler
import javax.activation.FileDataSource
import javax.mail.Message
import javax.mail.Session
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMessage
import javax.mail.internet.MimeMultipart
import java.text.SimpleDateFormat

static def toGermanFormat(def isoDate) {
  if (isoDate) {
    return new SimpleDateFormat("yyyy-MM-dd").with { parser ->
      new SimpleDateFormat("dd.MM.yyyy").format(
        parser.parse(isoDate)
      )
    }
  } else {
    return ""
  }
}

// e-mail
def sender = scriptArguments.emailSender
def password = ""
def host = "10.49.215.64"
def port = "25"

def recipients = scriptArguments.emailRecipients
def mailSubject = scriptArguments.emailSubject
def mailText = '''Sehr geehrter Geschäftspartner,
im Anhang finden Sie den unverbindlichen Auftragsforecast (=Status: noch nicht versendet) auf Basis der aktuell vorliegenden Informationen.

Bitte beachten Sie, dass sich dies in den kommenden Stunden/Tagen entsprechend verändern kann und nur eine Momentaufnahme darstellt.
Bei Rückfragen wenden Sie sich bitte direkt an den Ihnen bekannten Ansprechpartner im jeweiligen Lager (6278 = ab Lutterberg / 6288 = ab Kassel)
'''

// attachment
def filePrefix = "forecast"
def fileSuffix = ".xlsx"
def attachmentName = "forecast.xlsx"

def clientIdUser = clientIdUser
def serializer = new FilterConditionsSerializerJSON()
def filterConditionsSerialized = scriptArguments.filterConditions
def filterConditions = serializer.deserialize(filterConditionsSerialized)

def orderList = repoAccessorService.findDespatchOrders(clientIdUser, filterConditions)
def documentList = orderList.collect { elt -> elt.despatchOrderIntermediate.despatchOrderDocument.get(0) }

def workbook = new XSSFWorkbook()
XSSFSheet sheet = workbook.createSheet("sheet 1")
def tableHeader = ["Versandstelle", "Versanddienstleister", "Versandservice", "Auftragsnummer / Referenz",
                   "Bestellnummer", "Soll-Verladedatum", "Wunschlieferdatum", "Kundenname", "Straße/Haus Nr.",
                   "Empfänger PLZ", "Empfänger Ort", "Empfänger Tel.", "Auftragspos.", "Stück",
                   "Gesamt Kg", "Gesamt m³"]

def boldStyle = workbook.createCellStyle()
def font = workbook.createFont()
font.bold = true
boldStyle.setFont(font)
XSSFRow tableHeaderRow = sheet.createRow(0)

def cols = tableHeader.size()
for (def c = 0; c < cols; c++) {
  XSSFCell cell = tableHeaderRow.createCell(c)
  cell.setCellStyle(boldStyle)
  cell.setCellValue(tableHeader[c])
}

documentList.eachWithIndex { document, index ->
  XSSFRow row = sheet.createRow(index + 1)
  // Versandstelle
  XSSFCell cell00 = row.createCell(0)
  def value = document.partyInformation.find { it.partyQualifier == 'ShipFrom' }.internalNumber
  cell00.setCellValue(value)
  // Versanddienstleister
  XSSFCell cell01 = row.createCell(1)
  value = document.partyInformation.find { it.partyQualifier == 'Carrier' }.address.name
  cell01.setCellValue(value)
  // Versandservice
  XSSFCell cell02 = row.createCell(2)
  value = document.transportAndDeliveryTerms.despatchTypeCode
  cell02.setCellValue(value)
  // Auftragsnummer / Referenz
  XSSFCell cell03 = row.createCell(3)
  value = document.documentNumber
  cell03.setCellValue(value)
  //Bestellnummer
  XSSFCell cell04 = row.createCell(4)
  value = document.references.purchaseOrderNumber
  cell04.setCellValue(value)
  // Soll-Verladedatum
  XSSFCell cell05 = row.createCell(5)
  value = toGermanFormat(document.dateSpecification?.requestedDespatchDate?.date ?: "")
  cell05.setCellValue(value)
  // Wunschlieferdatum
  XSSFCell cell06 = row.createCell(6)
  value = toGermanFormat(document.dateSpecification?.requestedDeliveryDate?.date ?: "")
  cell06.setCellValue(value)
  // Kundenname
  XSSFCell cell07 = row.createCell(7)
  value = document.partyInformation.find { it.partyQualifier == 'ShipTo' }.address.name
  cell07.setCellValue(value)
  // Empfänger Straße
  XSSFCell cell08 = row.createCell(8)
  value = document.partyInformation.find { it.partyQualifier == 'ShipTo' }.address.street
  cell08.setCellValue(value)
  // Empfänger PLZ
  XSSFCell cell09 = row.createCell(9)
  value = document.partyInformation.find { it.partyQualifier == 'ShipTo' }.address.postalCode
  cell09.setCellValue(value)
  // Empfänger Ort
  XSSFCell cell10 = row.createCell(10)
  value = document.partyInformation.find { it.partyQualifier == 'ShipTo' }.address.city
  cell10.setCellValue(value)
  // Empfänger Tel.
  XSSFCell cell11 = row.createCell(11)
  value = document.partyInformation.find { it.partyQualifier == 'ShipTo' }.contact[0].telephoneNumber
  cell11.setCellValue(value)
  // Auftragspos.
  XSSFCell cell12 = row.createCell(12)
  value = document.lineItem.size()
  cell12.setCellValue(value)
  // Stück
  XSSFCell cell13 = row.createCell(13)
  value = document.lineItem.collect { lineItem ->
    Double.parseDouble(lineItem.lineItemQuantities.quantityToBeDelivered)
  }.sum()
  cell13.setCellValue(value)
  // Gesamt Kg
  XSSFCell cell14 = row.createCell(14)
  value = document.lineItem.collect { lineItem ->
    def quantity = Double.parseDouble(lineItem.lineItemQuantities.quantityToBeDelivered)
    def weight = Double.parseDouble(lineItem.lineItemDimensions.unitNetWeight)
    def weightUnitFactor = lineItem.lineItemDimensions.measurementUnitNetWeight.toKiloGramFactor
    weight * weightUnitFactor * quantity
  }.sum().round(0)
  cell14.setCellValue(value)
  // Gesamt m³
  XSSFCell cell15 = row.createCell(15)
  value = document.lineItem.collect { lineItem ->
    def quantity = Double.parseDouble(lineItem.lineItemQuantities.quantityToBeDelivered)
    def volume = Double.parseDouble(lineItem.lineItemDimensions.unitNetVolume)
    def volumeUnitFactor = lineItem.lineItemDimensions.measurementUnitNetVolume.toCubicMeterFactor
    volume * volumeUnitFactor * quantity
  }.sum().round(3)
  cell15.setCellValue(value)
}
for (def i = 0; i < tableHeader.size(); i++) {
  sheet.autoSizeColumn(i)
}

def tempFile = File.createTempFile(filePrefix, fileSuffix)
FileOutputStream outputStream = new FileOutputStream(tempFile)
workbook.write(outputStream)
outputStream.close()

Properties props = System.getProperties()
props.put("mail.smtp.user", sender)
props.put("mail.smtp.host", host)
props.put("mail.smtp.port", port)

def session = Session.getInstance(props)
def message = new MimeMessage(session)
message.setFrom(new InternetAddress(sender))

recipients.forEach { recipient ->
  message.addRecipients(Message.RecipientType.TO, new InternetAddress(recipient))
}

def multipart = new MimeMultipart()
def messageBodyPart = new MimeBodyPart()
messageBodyPart.setText(mailText)
multipart.addBodyPart(messageBodyPart)

def attachmentBodyPart = new MimeBodyPart()
attachmentBodyPart.setDataHandler(new DataHandler(new FileDataSource(tempFile)))
attachmentBodyPart.setFileName(attachmentName)
multipart.addBodyPart(attachmentBodyPart)

message.setSubject(mailSubject)
message.setContent(multipart)

Transport transport = session.getTransport("smtp")
transport.connect(host, Integer.valueOf(port), null, null)
transport.sendMessage(message, message.getAllRecipients())
transport.close()

tempFile.delete()

return ''
